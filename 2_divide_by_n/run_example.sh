#!/bin/bash

# Create the bin directory if it doesn't exist
mkdir -p bin

# Compile the Go code and place the binary in the bin folder
go build -o bin/divideAndPrint main.go

# Run the compiled binary with the desired parameters
./bin/divideAndPrint -start 9 -divisor 2
