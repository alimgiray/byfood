package main

import (
	"bytes"
	"fmt"
	"testing"
)

func TestDivideAndPrint(t *testing.T) {
	var buf bytes.Buffer
	expected := "9\n4\n2\n"

	divideAndPrintWithOutput(9, 2, &buf)

	if buf.String() != expected {
		t.Errorf("Expected %v but got %v", expected, buf.String())
	}
}

func divideAndPrintWithOutput(n, divisor int, buf *bytes.Buffer) {
	if n <= 1 {
		return
	}
	fmt.Fprintln(buf, n)
	divideAndPrintWithOutput(n/divisor, divisor, buf)
}
