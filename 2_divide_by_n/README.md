# Divide and Print

This project provides a command-line tool that takes a starting number and a divisor as input. It recursively divides the starting number by the divisor and prints the result at each step until the result is less than or equal to one.

## How It Works

The program takes two command-line flags:
- `-start`: The starting number (default is 9).
- `-divisor`: The number to divide by (default is 2).

It then recursively divides the starting number by the divisor, printing the result at each step, until the result is less than or equal to one.

## Usage

Compile the Go code:

```bash
go build -o bin/divideAndPrint main.go
```

Run the compiled binary with the desired starting number and divisor:

```bash
./bin/divideAndPrint -start 9 -divisor 2
```

You can also use the provided bash script to compile and run the example:

```bash
./run_example.sh
```

## Example

Input:

```bash
./bin/divideAndPrint -start 9 -divisor 2
```

Output:

```plaintext
9
4
2
```

## Development

The code is written in Go and makes use of command-line flags to take user input. The main logic is implemented in the `divideAndPrint` function, which recursively divides the given number by the specified divisor.

The compiled binary is placed in the `bin` folder, and a bash script is provided to compile and run the program.

## Testing

A test function is provided in `main_test.go` to test the `divideAndPrint` function. It captures the standard output and compares it to the expected result. You can use following command to run tests:

```bash
go test
```

## License

This project is open-source and available under the MIT License.
