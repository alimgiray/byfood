package main

import (
	"flag"
	"fmt"
)

func divideAndPrint(n, divisor int) {
	if n <= 1 {
		return
	}
	fmt.Println(n)
	divideAndPrint(n/divisor, divisor)
}

func main() {
	var startingNumber int
	var divisor int
	flag.IntVar(&startingNumber, "start", 9, "The starting number")
	flag.IntVar(&divisor, "divisor", 2, "The number to divide by")
	flag.Parse()

	divideAndPrint(startingNumber, divisor)
}
