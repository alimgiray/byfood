# Most Repeated Word Finder

This project provides a command-line tool that takes a comma-separated list of words and finds the most repeated word within the list.

## How It Works

The program takes two command-line flags:
- `-list`: A comma-separated list of words.

It then either finds and prints the most repeated word in the list or counts and prints the occurrences of the specified word.

## Usage

Compile the Go code:

```bash
go build -o bin/mostRepeated main.go
```

Run the compiled binary with the desired list of words:

```bash
./bin/mostRepeated -list "apple,pie,apple,red,red,red"
```

You can also use the provided bash script to compile and run the example:

```bash
./run_example.sh
```

## Example

Input:

```bash
./bin/mostRepeated -list "apple,pie,apple,red,red,red"
```

Output:

```plaintext
Most repeated word: red
```

## Development

The code is written in Go and makes use of command-line flags to take user input. The main logic is implemented in the `mostRepeated` function, which iterates through the given list and finds the most repeated word.

The compiled binary is placed in the `bin` folder, and a bash script is provided to compile and run the program.

## Testing

You can use following command to run tests:

```bash
go test
```

## License

This project is open-source and available under the MIT License.
