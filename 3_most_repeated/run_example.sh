#!/bin/bash

# Create the bin directory if it doesn't exist
mkdir -p bin

# Compile the Go code and place the binary in the bin folder
go build -o bin/mostRepeated main.go

# Run the compiled binary with the desired list of words
./bin/mostRepeated -list "apple,pie,apple,red,red,red"
