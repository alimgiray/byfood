package main

import (
	"testing"
)

func TestMostRepeated(t *testing.T) {
	tests := []struct {
		data     []string
		expected string
	}{
		{[]string{"apple", "pie", "apple", "red", "red", "red"}, "red"},
		{[]string{"a", "b", "b", "a", "a"}, "a"},
		{[]string{"cat", "dog", "cat", "cat"}, "cat"},
		{[]string{}, ""},
	}

	for _, test := range tests {
		actual := mostRepeated(test.data)
		if actual != test.expected {
			t.Errorf("Expected %v but got %v", test.expected, actual)
		}
	}
}
