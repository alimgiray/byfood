package main

import (
	"flag"
	"fmt"
	"strings"
)

func mostRepeated(data []string) string {
	counts := make(map[string]int)
	var mostRepeatedValue string
	maxCount := 0

	for _, value := range data {
		counts[value]++
		if counts[value] > maxCount {
			maxCount = counts[value]
			mostRepeatedValue = value
		}
	}

	return mostRepeatedValue
}

func main() {
	var list string
	flag.StringVar(&list, "list", "apple,pie,apple,red,red,red", "Comma-separated list of words")
	flag.Parse()

	data := strings.Split(list, ",")

	fmt.Println("Most repeated word:", mostRepeated(data))
}
