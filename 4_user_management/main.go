package main

import (
	"net/http"
)

func main() {
	http.HandleFunc("/", usersHandler)                   // Returns HTML for listing all users
	http.HandleFunc("/new", newUserHandler)              // Returns HTML form for creating new user
	http.HandleFunc("/edit", editUserHandler)            // Updates user and returns HTML result
	http.HandleFunc("/delete", deleteUserHandler)        // Deletes user and returns HTML result
	http.HandleFunc("/save", saveUserHandler)            // Creates / updates user and returns HTML result
	http.HandleFunc("/api/user", handleUserRequest)      // Handles GET for all users and POST for creating a user
	http.HandleFunc("/api/user/", handleUserByIDRequest) // Handles GET, PUT, and DELETE for a specific user by ID

	http.ListenAndServe(":8080", nil)
}
