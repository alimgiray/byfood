import axios from 'axios';

export const getAllUsers = () => axios.get('/api/user');
export const getUserByID = (id) => axios.get(`/api/user/${id}`);
export const saveUser = (user) => axios.post('/api/user', user);
export const updateUser = (id, user) => axios.put(`/api/user/${id}`, user);
export const deleteUser = (id) => axios.delete(`/api/user/${id}`);
