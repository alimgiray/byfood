import React from 'react';

function Header({ onNew }) {
  return (
    <div className="flex justify-between items-center p-4">
      <h1 className="text-2xl mb-4">
        <a href="/">User Management</a>
      </h1>
      <button onClick={onNew} className="bg-blue-500 text-white px-4 py-2">
        New
      </button>
    </div>
  );
}

export default Header;
