import React, { useState } from 'react';

function UserForm({ user, onSave, onCancel }) {
  const [name, setName] = useState(user.Name || '');
  const [email, setEmail] = useState(user.Email || '');

  const handleSubmit = (e) => {
    e.preventDefault();
    onSave({ ...user, Name: name, Email: email });
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="mb-4">
        <label htmlFor="name" className="block text-sm font-medium text-gray-600">Name:</label>
        <input
          type="text"
          name="name"
          value={name}
          onChange={(e) => setName(e.target.value)}
          required
          className="mt-1 p-2 w-full border rounded-md"
        />
      </div>
      <div className="mb-4">
        <label htmlFor="email" className="block text-sm font-medium text-gray-600">Email:</label>
        <input
          type="email"
          name="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
          className="mt-1 p-2 w-full border rounded-md"
        />
      </div>
      <button type="submit" className="bg-green-500 text-white px-4 py-2">Save</button>
      <button onClick={onCancel} className="bg-gray-500 text-white px-4 py-2">Back</button>
    </form>
  );
}

export default UserForm;
