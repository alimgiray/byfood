import React from 'react';

function UserTable({ users, onEdit, onDelete }) {
  return (
    <table className="min-w-full bg-white">
      <thead>
        <tr>
          <th className="py-2 text-center">ID</th>
          <th className="py-2 text-center">Name</th>
          <th className="py-2 text-center">Email</th>
          <th className="py-2 text-center">Actions</th>
        </tr>
      </thead>
      <tbody>
        {users.map((user) => (
          <tr key={user.ID}>
            <td className="py-2 text-center">{user.ID}</td>
            <td className="py-2 text-center">{user.Name}</td>
            <td className="py-2 text-center">{user.Email}</td>
            <td className="py-2 text-center">
              <button onClick={() => onEdit(user.ID)} className="bg-yellow-500 text-white px-2 py-1">Edit</button>
              <button onClick={() => onDelete(user.ID)} className="bg-red-500 text-white px-2 py-1">Delete</button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export default UserTable;
