import React, { useState, useEffect } from 'react';
import UserTable from './components/UserTable';
import UserForm from './components/UserForm';
import Header from './components/Header';
import * as userService from './services/userService';

function App() {
  const [users, setUsers] = useState([]);
  const [selectedUser, setSelectedUser] = useState(null);

  // Fetch users when the component mounts
  useEffect(() => {
    userService.getAllUsers().then((response) => setUsers(response.data));
  }, []);

  const handleEdit = (id) => {
    userService.getUserByID(id).then((response) => setSelectedUser(response.data));
  };

  const handleDelete = (id) => {
    userService.deleteUser(id).then(() => {
      setUsers(users.filter((user) => user.ID !== id));
    });
  };

  const handleSave = (user) => {
    if (user.ID) {
      userService.updateUser(user.ID, user).then(() => {
        setUsers(users.map((u) => (u.ID === user.ID ? user : u)));
      });
    } else {
      userService.saveUser(user).then((response) => {
        setUsers([...users, response.data]);
      });
    }
    setSelectedUser(null);
  };

  const handleCancel = () => {
    setSelectedUser(null);
  };

  return (
    <div>
      <Header onNew={() => setSelectedUser({})} />
      {selectedUser ? (
        <UserForm user={selectedUser} onSave={handleSave} onCancel={handleCancel} />
      ) : (
        <UserTable users={users} onEdit={handleEdit} onDelete={handleDelete} />
      )}
    </div>
  );
}

export default App;
