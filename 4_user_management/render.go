package main

import (
	"html/template"
	"net/http"
)

func renderUpdatedUserTable(w http.ResponseWriter, users []User) {
	tmpl := template.Must(template.ParseFiles("templates/user-table.html"))
	w.Write([]byte("<div id='user-table-container'>"))
	tmpl.ExecuteTemplate(w, "userTable", users)
	w.Write([]byte("</div><div id='detail-container'></div>"))
}

func renderUserDetails(w http.ResponseWriter, data map[string]interface{}) {
	tmpl := template.Must(template.ParseFiles("templates/detail.html"))
	tmpl.Execute(w, data)
}

func renderUserTable(w http.ResponseWriter, users []User) {
	tmpl := template.Must(template.ParseFiles("templates/index.html", "templates/user-table.html"))
	tmpl.Execute(w, users)
}
