package main

import (
	"log"
	"net/http"
	"strconv"
)

func usersHandler(w http.ResponseWriter, r *http.Request) {
	users, err := getUsersFromDB()
	if err != nil {
		log.Printf("Error while getting users from database: %s", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	renderUserTable(w, users)
}

func newUserHandler(w http.ResponseWriter, r *http.Request) {
	renderUserDetails(w, map[string]interface{}{"ActionButton": "Create"})
}

func editUserHandler(w http.ResponseWriter, r *http.Request) {
	idStr := r.URL.Query().Get("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		http.Error(w, "Invalid ID", http.StatusBadRequest)
		return
	}

	user, err := getUserFromDB(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	data := map[string]interface{}{
		"ID":           user.ID,
		"Name":         user.Name,
		"Email":        user.Email,
		"ActionButton": "Save",
	}

	renderUserDetails(w, data)
}

func saveUserHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	name := r.FormValue("name")
	email := r.FormValue("email")

	var err error
	switch r.Method {
	case http.MethodPost:
		user := User{Name: name, Email: email}
		err = saveUserToDB(&user)
	case http.MethodPut:
		idStr := r.FormValue("id")
		id, convertErr := strconv.Atoi(idStr)
		if convertErr != nil {
			http.Error(w, "Invalid ID", http.StatusBadRequest)
			return
		}
		user := User{ID: id, Name: name, Email: email}
		err = updateUserInDB(id, user)
	default:
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	users, err := getUsersFromDB()
	if err != nil {
		log.Printf("Error while getting users from database: %s", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	renderUpdatedUserTable(w, users)
}

func deleteUserHandler(w http.ResponseWriter, r *http.Request) {
	idStr := r.URL.Query().Get("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		http.Error(w, "Invalid ID", http.StatusBadRequest)
		return
	}

	if err = deleteUserFromDB(id); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	users, err := getUsersFromDB()
	if err != nil {
		log.Printf("Error while getting users from database: %s", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	renderUpdatedUserTable(w, users)
}
