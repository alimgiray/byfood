# User Management App

The User Management App is a simple web application that allows users to perform CRUD operations on user data. It includes both an htmx-based frontend and a React-based frontend, both of which interact with a Go backend.

## Features

- List all users in a table view
- Create new users
- Edit existing users
- Delete users

## Technologies Used

- Frontend: htmx and React
- Backend: Go
- Database: SQLite
- Styling: Tailwind CSS

## Getting Started

### Prerequisites

- Go (v1.16 or higher)
- Node.js (v14 or higher)
- npm or yarn

### Running the Backend

1. Navigate to the backend directory:
   ```bash
   cd backend
   ```

2. Run the Go server:
   ```bash
   go run main.go
   ```

### Running the htmx Frontend

The htmx frontend is served by the Go backend. Simply open your browser and navigate to `http://localhost:8080`.

### Running the React Frontend

1. Navigate to the react-frontend directory:
   ```bash
   cd react-frontend
   ```

2. Install the dependencies:
   ```bash
   npm install
   ```

3. Start the development server:
   ```bash
   npm start
   ```

4. Open your browser and navigate to `http://localhost:3000`.

## API Endpoints

The backend provides the following RESTful API endpoints:

- `GET /api/user`: Retrieve all users
- `GET /api/user/:id`: Retrieve a user by ID
- `POST /api/user`: Create a new user
- `PUT /api/user/:id`: Update a user by ID
- `DELETE /api/user/:id`: Delete a user by ID

## Why there are two frontends?

I have implemented two distinct frontends using htmx and React to showcase the differences in complexity and development experience between the two technologies. While React is a powerful and widely-used library for building user interfaces, it often requires a steeper learning curve and more lines of code (LoC). On the other hand, htmx offers a much simpler and more intuitive approach, allowing developers to write dynamic, modern web applications directly in HTML. This simplicity not only reduces the LoC but also enables backend developers, who may not be specialized in frontend development, to easily learn and write the HTML code. By providing both frontends, I aim to demonstrate that htmx can be a more efficient and accessible choice for teams with fewer dedicated frontend developers, without sacrificing the functionality and user experience of the application.

## License

This project is open-source and available under the MIT License.
