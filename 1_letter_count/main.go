package main

import (
	"flag"
	"fmt"
	"os"
	"sort"
	"strings"
)

func sortByLetterCount(words []string, letter string) []string {
	sort.Slice(words, func(i, j int) bool {
		countA_i := strings.Count(words[i], letter)
		countA_j := strings.Count(words[j], letter)

		// If the count of letters is the same, sort by length
		if countA_i == countA_j {
			return len(words[i]) > len(words[j])
		}

		// Otherwise, sort by the count of letters
		return countA_i > countA_j
	})

	return words
}

func main() {
	var letter string
	flag.StringVar(&letter, "letter", "a", "The letter to sort by")
	flag.Parse()

	words := flag.Args()
	if len(words) == 0 {
		fmt.Println("Please provide words as command-line arguments.")
		os.Exit(1)
	}

	sortedWords := sortByLetterCount(words, letter)
	fmt.Println(sortedWords)
}
