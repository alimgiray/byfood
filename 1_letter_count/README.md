# Word Sorting by Character Count

This project provides a command-line tool to sort a list of words based on the count of a specific character within each word. If some words contain the same count of the specified character, they are further sorted by their lengths in descending order.

## How It Works

The program takes a list of words and a character (specified by the `-letter` flag) as command-line arguments. It then sorts the words first by the count of occurrences of the specified character, and then by the length of the words if the counts are equal.

## Usage

Compile the Go code:

```bash
go build -o bin/sortwords main.go
```

Run the compiled binary with the words and the letter to sort by:

```bash
./bin/sortwords -letter "a" "word1" "word2" "word3" ...
```

You can also use the provided bash script to run the example:

```bash
./run_example.sh
```

## Example

Input:

```bash
./bin/sortwords -letter "a" "aaaasd" "a" "aab" "aaabcd" "ef" "cssssssd" "fdz" "kf" "zc" "lklklklklklklklkl" "l"
```

Output:

```plaintext
["aaaasd", "aaabcd", "aab", "a", "lklklklklklklklkl", "cssssssd", "fdz", "ef", "kf", "zc", "l"]
```

## Development

The code is written in Go and makes use of the standard library for sorting and string manipulation. The character to sort by is defined as a global variable, and command-line flags are parsed in the `init` function.

The compiled binary is placed in the `bin` folder, and the `.gitignore` file is configured to ignore this folder.

## Testing

You can use following command to run tests:

```bash
go test
```

## License

This project is open-source and available under the MIT License.
